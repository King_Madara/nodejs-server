import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {GameListComponent} from './componentes/game-list/game-list.component';
import {GameFormComponent} from './componentes/game-form/game-form.component';

const routes: Routes = [
  {
    /*Path es el nombre de la ruta*/
    path: '',
    /*Redireciona, para que cuando el cliente
    ingrese a la web lo primero que vea sea
    la lsta de juegos*/
    redirectTo: '/games',
    /*Nos quita un error*/
    pathMatch: 'full'
  },
  {
    /*Abre la ruta games*/
    path: 'games',
    /*AL visitar esta ruta va a renderizar
    el componente GameListComponent*/
    component: GameListComponent
  },
  {
    /*Cada que entre a la ruta
    games/add va a renderizar el
    componente GameFormComponent*/
    path: 'games/add',
    component: GameFormComponent
  },
  {
    path: 'games/edit/:id',
    component: GameFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
