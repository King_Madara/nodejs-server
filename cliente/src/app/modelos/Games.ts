export interface Juego{
    /*Todos los campos los colocaré
    opcionales para evitar errores
    en este tutorial*/
    id?: number;
    title?: string;
    descripcion?: string;
    imagen?: string;
    create_at?: Date;
}