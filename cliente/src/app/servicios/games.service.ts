import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Juego} from '../modelos/Games';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamesService {

  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  TenerJuegos(){
    return this.http.get(`${this.API_URI}/games`);
  }

  Juego(id:string){
    return this.http.get(`${this.API_URI}/games/${id}`);
  }

  BorrarJuego(id:string){
    return this.http.delete(`${this.API_URI}/games/${id}`);
  }

  /*Para guardar un juego necesitamos que ese juego
  sea un objeto de tipo Juego, o sea, que tenga los datos
  que indicamos en la interface*/
  GuardarJuego(game:Juego){
    return this.http.post(`${this.API_URI}/games`, game);
  }

  Actualizar(id:string|number, updateGame:Juego): Observable<Juego>{
    return this.http.put(`${this.API_URI}/games/${id}`, updateGame);
  }

}
