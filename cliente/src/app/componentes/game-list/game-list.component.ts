import { Component, HostBinding, OnInit } from '@angular/core';
import { Juego } from 'src/app/modelos/Games';
import {GamesService} from '../../servicios/games.service';
@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  /*encenrramos en un fila los juegos*/
  @HostBinding('class') classes= 'row';
  gms:any = [];

  constructor(private gamesService: GamesService) { }

  ngOnInit(): void {
    this.gamesService.TenerJuegos().subscribe(
      res => {
        this.gms= res
      },
      err => console.error(err)
    );
  }

  deleteGame(id:string){
    this.gamesService.BorrarJuego(id).subscribe(
      res =>{
        console.log(res);
        /*Para que Angular reinicie la vista
         de la ruta, sino el juego estara borrado
         pero para que se quite de la pantalla,
         el usuario debe recargar la pagina*/
        this.ngOnInit();
      },
      err => console.error(err)

    )
  }

}