import { Component, HostBinding, OnInit } from '@angular/core';
import { Juego } from 'src/app/modelos/Games';
import {GamesService} from '../../servicios/games.service';
import {ActivatedRoute,Router} from '@angular/router';

@Component({
  selector: 'app-game-form',
  templateUrl: './game-form.component.html',
  styleUrls: ['./game-form.component.css']
})
export class GameFormComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  game:Juego= {
    id: 0,
    title: "",
    descripcion: "",
    create_at: new Date()
  };

  constructor(private gamesService:GamesService, private route:Router, private activated:ActivatedRoute ) { }

  ngOnInit(): void {
    /*Esta Propiedad tiene los parammetros, tiene el id del juego
     que queremos editar*/
    const params= this.activated.snapshot.params;
    if (params) {
      this.gamesService.Juego(params['id']).subscribe(
          res => {
            console.log(res);
            this.game = res;
          },
          err => console.log(err)
        )
    }
  }

   makeNewGame(){
    delete this.game.create_at;
    delete this.game.id;

    this.gamesService.GuardarJuego(this.game).subscribe(
      res => {
        console.log(res);
        /*Una vez se haya creado el juego nos va a enviar
        a la ruta /games*/
        this.route.navigate(['/games']);
      },
      err => console.error(err)
    )
  }

}
