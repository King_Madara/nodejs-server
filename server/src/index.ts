import express, { Application, urlencoded } from 'express'
import indexGames from './rutas/indexGames';
import indexRoutes from './rutas/indexRoutes';
import morgan from 'morgan';
import cors from 'cors';

//Esta clase va a ejecutar
//el constructor que inicializa
//express
class Server{
    /*Indicamos que app pertenece a un tipo de dato
    de express llamado "Application"*/
    public app:Application;

    constructor(){
        /*La funcion express() inicia el programa
        express*/
        this.app= express();
        this.config();
        this.routes();
    }
    /*en esta funcion configuramos el puerto en el
    que queremos que abra la pagina*/
    config():void{
        /*"process.env.PORT" hace referencia al puerto
        por defecto, en caso de nuestro hostingg no nos
        proporcione de mas puertos
        
        Lo que dice esta instruccion es que incialice en
        el puerto que permita el hosting y si no hay
        uno preestablecido pues que abra en el puerto
        3mil*/
        this.app.set('port', process.env.PORT || 3000);
        /*Nos imprime en consola lo que pide
        el cliente (el que esta viendo la pagina)*/
        this.app.use(morgan('dev'));
        /*Permite conectar 2 servidores*/
        this.app.use(cors());
        /*Permite aceptar formatos Json de aplicaciones
        cliente. y responder igualmente a la peticion
        en formato Json*/
        this.app.use(express.json());
        /*este es en caso de que queramos enviar desde un
        formulario html*/
        this.app.use(urlencoded({extended:false}));
    }

    /*Esta funcion define las  rutas del servidor*/
    routes():void{
        this.app.use(indexRoutes);
        this.app.use(indexGames);
    }
    /*Esta funcion enciende el servidor*/
    start():void{
        /*Ejecutamos el servidor en el puerto que configuramos
        y luego que nos imprima el puerto en que estamos
        conectados*/
        this.app.listen(this.app.get('port'), () => {
            console.log("Server on port", this.app.get('port'));
        });
    }
}
const iniciar = new Server();
iniciar.start();