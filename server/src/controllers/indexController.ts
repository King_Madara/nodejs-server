import{Request, Response} from 'express';
import con from '../database_con';

class IndexController{
    public index(req: Request, res: Response){
        /*Podemos mandar archivos formato
        Json*/
        res.json('Entrada de la pagina');
    }
}
const ixController = new IndexController();
export default ixController;