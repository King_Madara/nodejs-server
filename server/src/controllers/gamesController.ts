import{json, Request, Response} from 'express';
import con from '../database_con';

class GamesController{
    public async Listagames(req: Request, res: Response): Promise<void>{
        const jugos= await con.query('SELECT * FROM games',
        (error,rows)=>{
            if(error) throw error
            res.json(rows);
        });
    }

    public async Onegame(req: Request, res: Response): Promise<void>{
        const {id}= req.params;
        const jugo= await con.query('SELECT * FROM games WHERE id = ?',
        [id],
        (error,rows)=>{
            if(error) throw error
            res.json(rows);
        });
        
    }

    public async Crear(req: Request,res: Response): Promise<void>{
        await con.query('INSERT INTO games set ?', [req.body]);
        res.json({message: 'Juego Creado y Guardado!'});
    }

    public async Actualizar(req: Request,res: Response){
        const { id } = req.params;
        await con.query('UPDATE games set ? WHERE id = ?', [req.body, id]);
        res.json({ message: "The game was Updated" });
    }

    public async Borrar(req: Request, res: Response): Promise<void>{
        const {id}= req.params;
        await con.query('DELETE FROM games WHERE id = ?', [id]);
        res.json({message: "El juego ha sido eliminado"});
    }
};
const gameController = new GamesController();
export default gameController;