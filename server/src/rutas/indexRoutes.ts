import {Router} from 'express';
import ixController from '../controllers/indexController';

class indexRoutes{
    /*De tipo router que pertenece
    a un método de express llamado
    Router*/
    public ruta:Router= Router();

    constructor(){
        this.config();
    };

    config():void{
        /*Una vez visiten nuestra ruta, va a devolver un
        mensaje que dice Hello*/
        this.ruta.get('/',ixController.index);
    };
}

const iRutas= new indexRoutes();
export default iRutas.ruta;