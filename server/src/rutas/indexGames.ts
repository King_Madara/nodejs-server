import {Router} from 'express';
import gameController from '../controllers/gamesController';

class gamesRoutes{
    /*De tipo router que pertenece
    a un método de express llamado
    Router*/
    public ruta:Router= Router();

    constructor(){
        this.config();
    };
        //Rutas
    config():void{
        this.ruta.get('/api/games', gameController.Listagames);
        this.ruta.get('/api/games/:id', gameController.Onegame);
        this.ruta.post('/api/games', gameController.Crear);
        this.ruta.put('/api/games/:id', gameController.Actualizar);
        this.ruta.delete('/api/games/:id', gameController.Borrar);
    };
}

const gamesRutas= new gamesRoutes();
export default gamesRutas.ruta;