"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importStar(require("express"));
const indexGames_1 = __importDefault(require("./rutas/indexGames"));
const indexRoutes_1 = __importDefault(require("./rutas/indexRoutes"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
//Esta clase va a ejecutar
//el constructor que inicializa
//express
class Server {
    constructor() {
        /*La funcion express() inicia el programa
        express*/
        this.app = (0, express_1.default)();
        this.config();
        this.routes();
    }
    /*en esta funcion configuramos el puerto en el
    que queremos que abra la pagina*/
    config() {
        /*"process.env.PORT" hace referencia al puerto
        por defecto, en caso de nuestro hostingg no nos
        proporcione de mas puertos
        
        Lo que dice esta instruccion es que incialice en
        el puerto que permita el hosting y si no hay
        uno preestablecido pues que abra en el puerto
        3mil*/
        this.app.set('port', process.env.PORT || 3000);
        /*Nos imprime en consola lo que pide
        el cliente (el que esta viendo la pagina)*/
        this.app.use((0, morgan_1.default)('dev'));
        /*Permite conectar 2 servidores*/
        this.app.use((0, cors_1.default)());
        /*Permite aceptar formatos Json de aplicaciones
        cliente. y responder igualmente a la peticion
        en formato Json*/
        this.app.use(express_1.default.json());
        /*este es en caso de que queramos enviar desde un
        formulario html*/
        this.app.use((0, express_1.urlencoded)({ extended: false }));
    }
    /*Esta funcion define las  rutas del servidor*/
    routes() {
        this.app.use(indexRoutes_1.default);
        this.app.use(indexGames_1.default);
    }
    /*Esta funcion enciende el servidor*/
    start() {
        /*Ejecutamos el servidor en el puerto que configuramos
        y luego que nos imprima el puerto en que estamos
        conectados*/
        this.app.listen(this.app.get('port'), () => {
            console.log("Server on port", this.app.get('port'));
        });
    }
}
const iniciar = new Server();
iniciar.start();
