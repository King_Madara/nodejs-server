"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexController_1 = __importDefault(require("../controllers/indexController"));
class indexRoutes {
    constructor() {
        /*De tipo router que pertenece
        a un método de express llamado
        Router*/
        this.ruta = (0, express_1.Router)();
        this.config();
    }
    ;
    config() {
        /*Una vez visiten nuestra ruta, va a devolver un
        mensaje que dice Hello*/
        this.ruta.get('/', indexController_1.default.index);
    }
    ;
}
const iRutas = new indexRoutes();
exports.default = iRutas.ruta;
