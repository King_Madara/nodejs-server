"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const gamesController_1 = __importDefault(require("../controllers/gamesController"));
class gamesRoutes {
    constructor() {
        /*De tipo router que pertenece
        a un método de express llamado
        Router*/
        this.ruta = (0, express_1.Router)();
        this.config();
    }
    ;
    //Rutas
    config() {
        this.ruta.get('/api/games', gamesController_1.default.Listagames);
        this.ruta.get('/api/games/:id', gamesController_1.default.Onegame);
        this.ruta.post('/api/games', gamesController_1.default.Crear);
        this.ruta.put('/api/games/:id', gamesController_1.default.Actualizar);
        this.ruta.delete('/api/games/:id', gamesController_1.default.Borrar);
    }
    ;
}
const gamesRutas = new gamesRoutes();
exports.default = gamesRutas.ruta;
