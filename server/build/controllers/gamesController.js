"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_con_1 = __importDefault(require("../database_con"));
class GamesController {
    Listagames(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const jugos = yield database_con_1.default.query('SELECT * FROM games', (error, rows) => {
                if (error)
                    throw error;
                res.json(rows);
            });
        });
    }
    Onegame(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const jugo = yield database_con_1.default.query('SELECT * FROM games WHERE id = ?', [id], (error, rows) => {
                if (error)
                    throw error;
                res.json(rows);
            });
        });
    }
    Crear(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_con_1.default.query('INSERT INTO games set ?', [req.body]);
            res.json({ message: 'Juego Creado y Guardado!' });
        });
    }
    Actualizar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_con_1.default.query('UPDATE games set ? WHERE id = ?', [req.body, id]);
            res.json({ message: "The game was Updated" });
        });
    }
    Borrar(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_con_1.default.query('DELETE FROM games WHERE id = ?', [id]);
            res.json({ message: "El juego ha sido eliminado" });
        });
    }
}
;
const gameController = new GamesController();
exports.default = gameController;
