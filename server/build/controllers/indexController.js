"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IndexController {
    index(req, res) {
        /*Podemos mandar archivos formato
        Json*/
        res.json('Entrada de la pagina');
    }
}
const ixController = new IndexController();
exports.default = ixController;
